/*
Ace Attorney Online - Load and use the soundmanager library

*/

//MODULE DESCRIPTOR
Modules.load(new Object({
	name : 'soundmanager',
	dependencies : [],
	init : function() {}
}));

//INDEPENDENT INSTRUCTIONS
window.SM2_DEFER = true;
includeScript('soundmanager/soundmanager2-nodebug-jsmin', false, '', function(){
	window.soundManager = new SoundManager();
	
	soundManager.setup({
		url: cfg['js_dir'] + 'soundmanager/soundmanager2.swf',
		
		flashVersion: 8,
		useFlashBlock: false,
		useHTML5Audio: true,
		preferFlash: true, // TODO: Change to false when Firefox latency issue is resolved : https://bugzilla.mozilla.org/show_bug.cgi?id=926244
		
		debugMode: false,
		useConsole: true,
		consoleOnly: true,
		
		onready: function(){
			//End of module once SM2 is loaded and configured
			Modules.complete('soundmanager');
		}
	});
	
	soundManager.beginDelayedInit();
});



//EXPORTED VARIABLES


//EXPORTED FUNCTIONS


//END OF MODULE
//Delayed until SM2 is asynchronously loaded
